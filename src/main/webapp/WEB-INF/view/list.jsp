<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>系统用户登录</title>
<link href="/resource/css/bootstrap.css" rel="stylesheet">
<script type="text/javascript" src="/resource/js/jquery-3.2.1.js"></script>
<script type="text/javascript" src="/resource/js/bootstrap.js"></script>
 <script type="text/javascript">
 function goPage(pageNum){
	 var orderMethod='${companyVO.orderMethod}';
	 var regionId ='${companyVO.regionId}';
	 
	 location.href="/selects?pageNum="+pageNum+"&orderMethod="+orderMethod+"&regionId="+regionId
 }
 
 
 //全选
 function chk(flag){
	 
	 if(flag==1){//全选
		 $("[name='ids']").prop("checked",true);
	 }else if(flag==2){//全不选
		 $("[name='ids']").prop("checked",false);
	 }else{//反选
		 $("[name='ids']").each(function(){
			this.checked=!this.checked;
		 })
	 }
 }
 
 //批量删除
 function deleteBatch(){
	 var ids=new Array();
	 //把选中的值放入数组
	 $("[name='ids']:checked").each(function(i){
		 ids[i] =$(this).val();
	 })
	 
	//如果没有选中，则进行提示，不调用后台方法
	if(ids.length==0){
		alert("至少选中一个");
		return ;
	}
	 var ret=confirm("确定删除吗？");
	 if(ret){
		 //执行后台删除
		 $.post("/deleteBatch",{ids:ids},function(flag){
			 if(flag){
				 alert("删除成功");
				 location.href="/selects";
			 }else{
				 alert("删除失败")
			 }
		 })
		 
		 
		 
	 }
	 
	
	 
	 
	 
	 
	 
 }
 </script>
</head>
<body>



 <a href="/add">增加</a>
 <button type="button" onclick="deleteBatch()">批量删除</button>
  按地区查询：
   <c:forEach items="${regions}" var="region">
   <a href="/selects?regionId=${region.id }"> ${region.name }</a>
   </c:forEach>
      <a href="/selects">显示全部</a>
   按批准日期排序：
   <a href="/selects?orderMethod=asc">正序</a>
   <a href="/selects?orderMethod=desc">倒序</a>
   <a href="/selects">取消排序</a>
   
	<table class="table">
		<tr>
		   
			<td>
			<button type="button" onclick="chk(1)">全选</button>
			<button type="button" onclick="chk(2)">全不选</button>
			<button type="button" onclick="chk(3)">反选</button>
			
			 </td>
			<td>序号</td>
			<td>中文名称</td>
			<td>英文名称</td>
			<td>国别</td>
			<td>银行</td>
			<td>批准日期</td>
			<td>操作</td>

		</tr>
		<c:forEach items="${info.list}" var="company" varStatus="index">
			<tr>
				<td><input type="checkbox" name="ids" value="${company.id }"></td>
				<td>${index.count }</td>
				<td>${company.cnname }</td>
				<td>${company.enname }</td>
				<td>${company.region.name }</td>
				<td>${company.bank }</td>
				<td><fmt:formatDate value="${company.approvaled }" pattern="yyyy-MM-dd"/> </td>
				<td><img alt="没有图片" src="/pic/${company.pic }" height="50px" width="50px"></td>
				<td>修改</td>
			</tr>

		</c:forEach>


	</table>
  <jsp:include page="/WEB-INF/view/pages.jsp"></jsp:include>
</body>
</html>