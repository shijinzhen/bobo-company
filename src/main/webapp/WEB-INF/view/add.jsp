<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>增加公司</title>
<link href="/resource/css/bootstrap.css" rel="stylesheet">
<script type="text/javascript" src="/resource/js/jquery-3.2.1.js"></script>
<script type="text/javascript" src="/resource/js/bootstrap.js"></script>
</head>
<body>

<form:form modelAttribute="company" action="/add" method="post" enctype="multipart/form-data">

中文名称： <form:input path="cnname"/>
 <form:errors path="cnname" cssClass="text-danger"></form:errors>
<br>

英文名称：<form:input path="enname"/>
<form:errors path="enname" cssClass="text-danger"></form:errors>
<br>

地区：
  <form:select path="regionId">
    <form:option value="">请选择</form:option>
   <form:options items="${regions}" itemLabel="name" itemValue="id" ></form:options>
  
  </form:select>
  <form:errors path="regionId" cssClass="text-danger"></form:errors>
  <br>
  托管银行：
  <form:input path="bank"/>
  <form:errors path="bank" cssClass="text-danger"></form:errors>
  <br>
 批准日期  <form:input path="approvaled" type="date"/>
 
 <form:errors path="approvaled" cssClass="text-danger"> </form:errors><br>
 
 图片：<input type="file" name="file"> 

 <form:errors path="pic" cssClass="text-danger"></form:errors><br>
 <button type="submit">保存</button>
</form:form>

</body>
</html>