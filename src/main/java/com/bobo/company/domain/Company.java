package com.bobo.company.domain;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

public class Company {

	private Integer id;
	@NotBlank(message = "公司中文名称不能为空")
	@Length(max = 20,min = 3,message = "长度应该在3-20位")
	private String cnname;
	@NotBlank(message = "公司中文名称不能为空")
	@Length(max = 20,min = 3,message = "长度应该在3-20位")
	private String enname;
	@NotNull(message = "所属地区不能问空")
	private Integer regionId;//地区ID 
	@NotBlank(message = "银行名称不能为空")
	@Length(max = 20,min = 3,message = "长度应该在3-20位")
	private String bank;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@NotNull(message = "日期不能为空")
	@Past(message = "日期必须过去日期")
	private Date approvaled;//批准日期
	
	private Region region;//地区
	
	private String pic;//图片
	
	
	
	public String getPic() {
		return pic;
	}
	public void setPic(String pic) {
		this.pic = pic;
	}
	public Region getRegion() {
		return region;
	}
	public void setRegion(Region region) {
		this.region = region;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCnname() {
		return cnname;
	}
	public void setCnname(String cnname) {
		this.cnname = cnname;
	}
	public String getEnname() {
		return enname;
	}
	public void setEnname(String enname) {
		this.enname = enname;
	}
	public Integer getRegionId() {
		return regionId;
	}
	public void setRegionId(Integer regionId) {
		this.regionId = regionId;
	}
	public String getBank() {
		return bank;
	}
	public void setBank(String bank) {
		this.bank = bank;
	}
	public Date getApprovaled() {
		return approvaled;
	}
	public void setApprovaled(Date approvaled) {
		this.approvaled = approvaled;
	}
	
	
}
