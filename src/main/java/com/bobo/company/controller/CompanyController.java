package com.bobo.company.controller;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.bobo.company.domain.Company;
import com.bobo.company.domain.Region;
import com.bobo.company.service.CompanyService;
import com.bobo.company.vo.CompanyVO;
import com.github.pagehelper.PageInfo;


//我是李四--我也改动了
//我的张三 -我又改了
@Controller
public class CompanyController {
	//优化
	@Resource
	private CompanyService companyService;
	
	//批量删除
	@ResponseBody
	@PostMapping("deleteBatch")
	public boolean deleteBatch(@RequestParam("ids[]") Integer[] ids) {
		
		return companyService.deleteBatch(ids)>0;
		
	}
	
	//列表
	@RequestMapping("selects")
	public String selects(Model model,CompanyVO companyVO,@RequestParam(defaultValue = "1")Integer pageNum
			,@RequestParam(defaultValue = "3")Integer pageSize) {
		
		PageInfo<Company> info = companyService.selects(companyVO, pageNum, pageSize);
		model.addAttribute("info", info);
		model.addAttribute("companyVO", companyVO);
		
		
		//查询所有地区。
		List<Region> regions = companyService.selectRegions();
		model.addAttribute("regions", regions);
		
		return "list";
	}
	

	//去增加Company  company
	@GetMapping("add")
	public String add(Model model,Company company) {
		//查询所有地区。
		List<Region> regions = companyService.selectRegions();
		model.addAttribute("regions", regions);
		return "add";
	}
	
	
	//执行增加
	@PostMapping("add")
	public String add(Model model,@Valid Company company,BindingResult reuslt,MultipartFile file) {
		
		if(file==null ||file.isEmpty()) {//如果没有上传文件。则进行消息提示
			reuslt.rejectValue("pic", null, "图片必须上传");
		}
		if(reuslt.hasErrors()) {//如果有错误消息则回到增加页面显示错误消息
			//查询所有地区。
			//查询所有地区。
			List<Region> regions = companyService.selectRegions();
			model.addAttribute("regions", regions);
		
			return "add";
		}
		//文件上传
		String path="d:/pic/";
		//aaa.jpg
		String originalFilename = file.getOriginalFilename();
		// .jpg
	 String filename = UUID.randomUUID() +originalFilename.substring(originalFilename.lastIndexOf("."));
		
		
		File f = new File(path,filename);
		try {
			file.transferTo(f);
			
			company.setPic(filename);
			
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		companyService.insert(company);
		
		return "redirect:/selects";
		
	}
	
}
