package com.bobo.company.dao;

import java.util.List;

import com.bobo.company.domain.Company;
import com.bobo.company.domain.Region;
import com.bobo.company.vo.CompanyVO;

public interface CompanyMapper {
	
	//批删除
	int deleteBatch(Integer[] ids);
	
	//列表查询
	List<Company>selects(CompanyVO companyVO);
	
	
	//增加
	int insert(Company company);

	//查询所有的地区
	List<Region> selectRegions();
}
