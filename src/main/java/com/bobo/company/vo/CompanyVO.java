package com.bobo.company.vo;

import com.bobo.company.domain.Company;

public class CompanyVO extends Company {
	
	private String orderMethod;//asc desc

	public String getOrderMethod() {
		return orderMethod;
	}

	public void setOrderMethod(String orderMethod) {
		this.orderMethod = orderMethod;
	}
	
	

}
