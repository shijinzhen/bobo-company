package com.bobo.company.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.bobo.company.dao.CompanyMapper;
import com.bobo.company.domain.Company;
import com.bobo.company.domain.Region;
import com.bobo.company.service.CompanyService;
import com.bobo.company.vo.CompanyVO;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

@Service
public class CompanyServiceImpl implements CompanyService {

	@Resource
	CompanyMapper companyMapper;
	@Override
	public int insert(Company company) {
		// TODO Auto-generated method stub
		return companyMapper.insert(company);
	}
	@Override
	public List<Region> selectRegions() {
		// TODO Auto-generated method stub
		return companyMapper.selectRegions();
	}
	@Override
	public PageInfo<Company> selects(CompanyVO companyVO, Integer pageNum, Integer pageSize) {
		PageHelper.startPage(pageNum, pageSize);
		List<Company> list = companyMapper.selects(companyVO);
		return new PageInfo<Company>(list);
	}
	@Override
	public int deleteBatch(Integer[] ids) {
		// TODO Auto-generated method stub
		return companyMapper.deleteBatch(ids);
	}

}
