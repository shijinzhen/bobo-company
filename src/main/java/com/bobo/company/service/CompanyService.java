package com.bobo.company.service;

import java.util.List;

import com.bobo.company.domain.Company;
import com.bobo.company.domain.Region;
import com.bobo.company.vo.CompanyVO;
import com.github.pagehelper.PageInfo;

public interface CompanyService {
	
	//批删除
		int deleteBatch(Integer[] ids);
	//列表查询
	PageInfo<Company>selects(CompanyVO companyVO,Integer pageNum,Integer pageSize);
	//查询所有的地区
		List<Region> selectRegions();

	//增加
		int insert(Company company);
}
